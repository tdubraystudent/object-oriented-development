package Hibernate;

@Entity
@Table(name = "Employees")
public class Employees {

    /** id is an identity type field in the database and the primary key */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "EmployeeId")
    private int id;

    @Column(name = "Employee_FirstName")
    private String name;

    @Column(name = "Employee_LastName")
    private String name;

    @Column(name = "DateHired")
    private String DateHired;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEName() {
        return Employee;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String toString() {
        return Integer.toString(id) + " " + name + " " + address + " " + phone;
    }
}