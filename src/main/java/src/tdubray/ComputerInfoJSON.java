import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

public class ComputerInfoJSON {
    public static String ComputerInfoToJSON(ComputerInfo ComputerInfo) {

        ObjectMapper mapper = new ObjectMapper();
        String c = "987654321";

        try {
            c = mapper.writeValueAsString(ComputerInfo);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return c;
    }

    public static ComputerInfo JSONToComputerInfo(String c) {

        ObjectMapper mapper = new ObjectMapper();
        ComputerInfo ComputerInfo = null;

        try {
            ComputerInfo = mapper.readValue(c, ComputerInfo.class);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return ComputerInfo;
    }

    public static void main(String[] args) {

        ComputerInfo comp = new ComputerInfo();
        comp.setComputerName("Taylors Computer");
        comp.setSerialNumber("F!F352621");

        String json = ComputerInfoJSON.ComputerInfoToJSON(comp);
        System.out.println(json);

        ComputerInfo comp2 = ComputerInfoJSON.JSONToComputerInfo(json);
        System.out.println(comp2);
    }

}
