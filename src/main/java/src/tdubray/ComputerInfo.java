public class ComputerInfo {
    private String ComputerName;
    private long SerialNumber;

    public String getComputerName() {
        return ComputerName;
    }

    public void setComputerName(String ComputerName) {
        this.ComputerName = ComputerName;
    }

    public long SerialNumber() {
        return SerialNumber;
    }

    public void setSerialNumber(long IpAddress) {
        this.SerialNumber = SerialNumber;
    }

    public String toString() {
        return "Computer Name: " + ComputerName + "Serial Number: : " + SerialNumber;
    }
}

