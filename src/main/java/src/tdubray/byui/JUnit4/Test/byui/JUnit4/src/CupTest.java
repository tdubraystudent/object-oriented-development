package byui.JUnit4.src;

import org.junit.Test;

import static org.testng.Assert.assertEquals;

public class CupTest {

    @Test
    void getLiquidType() {
        Cup c = new Cup(getLiquidType(); "Orange Juice", percentFull 55.43);
        assertEquals("Orange Juice", c.getLiquidType());
    }

}