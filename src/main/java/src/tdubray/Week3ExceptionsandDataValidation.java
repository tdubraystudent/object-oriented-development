import java.util.Scanner;

public class Week3ExceptionsandDataValidation {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int x = 1;

        do {
            try {
                System.out.println("Enter first number: ");
                int n1 = input.nextInt();
                System.out.println("Enter second number: ");
                int n2 = input.nextInt();
                int sum = n1 / n2;
                System.out.println(sum);
                x = 2;
            } catch (ArithmeticException e) {
                System.out.println("You have entered something that = 0, please try again!");
            }
        } while (x == 1);
    }
}
