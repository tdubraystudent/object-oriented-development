package Serlvet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "SuperManServlet", urlPatterns =("/Servlet"))
public class SuperManServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        response.setContentType("<html><head></head><body>");
        String username = request.getParameter( "Admin Username");
        String password = request.getParameter( "Admin Password");
        out.println("<h3>Superman Secret Login Information");
        out.println("<p>Admin Username: " +  username + "<p>");
        out.println("<p>Admin Password: " + password + "<p>");
        out.println("</body></html>");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println(" You are not the real Superman please contact Clark Kent!");
    }
}
