package Hibernate;

import javax.persistence.*;

@Entity
@Table(name = "Employees")
public class Employees {

    /** id is an identity type field in the database and the primary key */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "EmployeeId")
    private int id;

    @Column(name = "Employee_FirstName")
    private String name;

    @Column(name = "Employee_FirstName")
    private String getName;

    @Column(name = "DateHired")
    private String DateHired;
    private Object Date;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getDateHired() {
        return DateHired;
    }

    public void setDateHired(String DateHired) {
        this.DateHired = DateHired;
    }

    public String toString() {
        return Integer.toString(id) + " " + name + " " + getDateHired() + " " + Date;
    }
}