package Hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import java.util.*;

/** TestDAO implemented using a singleton pattern
 *  Used to get customer data from my MYSQL database*/
public class TaylorsTestDAO {

    SessionFactory factory = null;
    Session session = null;

    private static TaylorsTestDAO single_instance = null;

    private TaylorsTestDAO()
    {
        factory = HibernateUtils.getSessionFactory();
    }

    /** This is what makes this class a singleton.  You use this
     *  class to get an instance of the class. */
    public static TaylorsTestDAO getInstance()
    {
        if (single_instance == null) {
            single_instance = new TaylorsTestDAO();
        }

        return single_instance;
    }

    /** Used to get more than one customer from database
     *  Uses the OpenSession construct rather than the
     *  getCurrentSession method so that I control the
     *  session.  Need to close the session myself in finally.*/
    public List<Employees> getCustomers() {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from Hibernate.Employees";
            List<Employees> EE = (List<Employees>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return EE;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }

    }

    /** Used to get a single customer from database */
    public Employees getEmployees(int id) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from = Hibernate.Employees where id=" + Integer.toString(id);
            Employees E = (Employees) session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return E;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

}